let promise = new Promise(function(resolve,reject){
    // setTimeout(()=>resolve('done'), 1000);
    setTimeout(()=>reject(new Error()), 1000);

});

// promise.then(
//     result=>console.log(result),
//     error => console.log(error)
// );

promise.catch(alert);

let promise = new Promise(function(resolve,reject){
    resolve(1);
})

promise
    .then(function(result){console.log(result);return result*2})
    .then(function(result){console.log(result);return result*2})
    .then(function(result){console.log(result);return result*2})
    .then(function(result){console.log(result);return result*2});

function delay(ms) {
    return new Promise(function(resolve, reject){
        setTimeout(()=>alert(`${Math.round(ms/1000)}s`), ms);
    })
}

delay(1000).then();

function loadScript(url) {
    return new Promise(function (resolve, reject) {
        let script = document.createElement('script');
        script.setAttribute('src',url);
        document.body.appendChild(script);
        resolve();
    });
}

let jqueryScript = loadScript('https://code.jquery.com/jquery-3.3.1.min.js');

jqueryScript.then(
    result => alert('script is loaded'),
    error => alert('error loading script')
);

async function func() {
    return 1;
}

func().then(result => console.log(result));

async function f() {
    let promise = new Promise((resolve, reject) => {
        setTimeout(() => resolve("done!"), 1000)
    });
    let result = await promise;
    alert(result);
}
f();



