## Lesson 1 - Introduce to frontend

### Topic 1.  Вступна інфорормація

Базові метеріали:  
[Codecademy по основам html та css (анг)](https://www.codecademy.com/tracks/web)  
[Початок роботи з Вебом](https://developer.mozilla.org/ru/docs/Learn/Getting_started_with_the_web)  
[Як працює Веб](https://developer.mozilla.org/ru/docs/Learn/Getting_started_with_the_web/How_the_Web_works)   
[What is front-end web development](https://generalassemb.ly/blog/what-is-front-end-web-development/)  
[Курс по браузерним інструментам від Code School](http://discover-devtools.codeschool.com/)  
[Історія HTML wiki](https://ru.wikipedia.org/wiki/HTML)  
[What is Page Load Time](https://www.maxcdn.com/one/visual-glossary/page-load-time/)  
[Робота з мережею](https://habr.com/company/first/blog/348686/)  

**Фронтенд** (англ. *Front-end*) и **бекенд** (англ. *back-end*) — варіант архітектури програмного забезпечення (ПЗ). 
Це терміни в програмній інженерії, які відрізняються відповідно до принципу розділення відповідальності між рівнем представлення і рівнем доступу до 
даних відповідно.   
*Front-end* — інтерфейс взаємодії між користувачем та основною програмно-апаратною частиною (*back-end*), іншими словами - клієнтська сторона.
*Front-end* и *back-end* можуть бути розподілені між однією або декількома системами.    
Фронтенд технології "живуть" в браузері. Після того, як ви вбиваєте URL в адресний рядок і натискаєте Enter, ваш браузер отримує HTML-файл з веб-сервера. 
Цей файл також запропонує браузеру запросити у сервера додаткові *CSS* і *Javascript* файли (як правило - багато файлів).
Кожну з цих мов (HTML, CSS, Javascript) можна розділити на наступні функціональні, **СТРУКТУРА (HTML)** веб-сторінки, як вона **ВИГЛЯДАЄ (CSS)** і як **ФУНКЦІОНУЄ (Javascript)**.
Пам'ятайте, що саме браузер (а не сервер) збирає ці файли в одну функціональну веб-сторінку.

**Hypertext Markup Language** (*HTML*, мова розмітки гіпертексту) — це код, що структурує зміст веб-сторінок та надає йому значення і мети. Наприклад, чи є контент набором параграфів, чи списком із маркерами? Чи є на сторінці зображення, таблиці з даними?
Мова HTML інтерпретується браузерами, отриманий в результаті інтерпретації форматований текст відображається на екрані монітора комп'ютера або мобільного пристрою.

**Cascading Stylesheets** (*CSS*, каскадні таблиці стилів) — це код для стилізації сайту. Наприклад, який колір тексту, де розміщенно контент на екрані? Які фонові зображення та кольори взято для оздоблення сайту? 

**JavaScript** — це мова програмування, яку використовують для можливості взаємодії з веб-сайтами. 

### Topic 2.  Порядок завантаження HTML сторінки
[Послідовність завантаження веб-сторінки (stackoverflow)](https://stackoverflow.com/questions/28635141/sequence-in-which-html-page-loads)    
[Пояснення параметрів консолі](https://developers.google.com/web/tools/chrome-devtools/network-performance/reference#timing-explanation)
[RFC опис специфікації по script](https://html.spec.whatwg.org/multipage/scripting.html#scriptingLanguages)  
[Принципи работи сучасних веб-браузерів (html5rocks)](https://www.html5rocks.com/ru/tutorials/internals/howbrowserswork/#The_browsers_we_will_talk_about)
     
На прикладі наведеному у папці `topic1` провести аналіз послідовності завантаження елементів сторінки (використовуючи консоль розробника браузера Chrome).  

##### Підсумкова інформація. 
Сторінка HTML аналізується послідовно від початку до кінця. Браузер запускає паралельні запити для ресурсів такі як таблиці стилів, зображення або сценарії.  
Зображення і таблиці стилів не є блокуючими ресурсами, що означає, що синтаксичний аналіз решти сторінки може тривати, поки ці ресурси завантажуються.
Script теги, які зазначені без атрибутів `defer` або `async`, блокуються, і вони повинні завантажитись і виконатись до продовження аналізу сторінки.  
Таблиці стилів завантажуються паралельно, і сторінка не блокує подальший синтаксичний розбір, чекаючи завантаження таблиці стилів.
Зображення завантажуються асинхронно і не блокують завантаження решти сторінки або HTML-тегів.

Послідновність операцій:

+ Браузер аналізує теги `<html>` и `<head>`.
+ Браузер зустрічає перший тег `<link>`, бачить посилання на зовнішню таблицю стилів та ініціює запит на сервер для завантаження цієї зовнішньої таблиці стилів. Браузер не чекає завершеня цього запиту.
+ Браузер зустрічає другий тег `<link>`, бачить посилання на зовнішню таблицю стилів та ініціює запит на сервер для завантаження другої зовнішньої таблиці стилів. Браузер не чекає завершеня цього запиту.
+ Браузер зустрічає перший тег `<script>`, що вказує на зовнішній script файл. Браузер ініціює запит на сервер для зовнішнього файлу script.
+ Браузер може "дивитися вперед" і бачити наструпний тег `<script>`, і також ініціювати запит для цього зовнішнього файлу script.
+ Оскільки  зовнішні ресурси `<script>` є блокуючими ресурсами, официальний (передбачений RFC стандартами) синтаксичний аналіз і виконання сторінки не може продовжуватися до тих пір, пока не будет завантажений і виконаний перший файл script. Браузер может дивитися вперед, щоб дізнатися, чи потрібно запускати завантаження інших ресурсів.
+ Перший файл script завершає завантаження. Браузер виконує цей script.
+ Другий файл script завершає завантаження. Браузер виконує цей script.
+ В любий момент цього процесу, якщо довільна із таблиць стилів завершує завантаження, вони обрабляються, як тільки вони будуть доступні.
+ Після обробки другого блоку script продовжується парсинг странки.
+ Теги `</head>` и `<body>` анализуються.
+ `<button>button1</button>` аналізується і добавляється в тіло DOM. На даний момент браузер може виконувати часткову візхуалізацію.
+ Теги `<img>` аналізуються та ініціюються завантаження для зовнішніх зображень.
+ Другий тег `<button>` обробляється і може бути вже відрендереним.
+ Браузер бачить завершальні теги, що означають кінец сторінки.
+ В якийсь момент в майбутньому завершується завантаження  та рендер зображень.


### Topic 3. Основи HTML
[Історія HTML(wiki)](https://ru.wikipedia.org/wiki/HTML)  
[Basics w3schools](https://www.w3schools.com/html/default.asp)  
[Basics mozilla.org](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started)

Що таке HTML?  
HTML це стандартна мова розмітки для створення веб-сторінок.
Основні тези, що стосуються HTML 
+ HTML абревіатура від  **Hyper Text Markup Language**  (мова гіпертекстової розмітки)
+ HTML описує структуру веб-сторінок за допомогою розмітки
+ HTML елементи є будівельними блоками HTML-сторінок 
+ HTML елементи представлені тегами
+ HTML елементи представляють частини контенту такі як "heading", "paragraph", "table", і так далі
+ Браузери  не відображають HTML теги, але використовують для рендеру вмісту сторінки.

Структура простого HTML документу 
```html
<!DOCTYPE html>
<html>
    <head>
    <title>Page Title</title>
</head>
<body>
    <h1>Heading</h1>
    <p>Paragraph</p>
</body>
</html>
```

+ `<!DOCTYPE html>` визначає цей документ як стандарт HTML5
+ `<html>` Елемент є кореневим елементом HTML сторінки
+ `<head>` Елемент містить мета-інформацію про документ
+ `<title>` Елемент визначає заголовок для документа
+ `<body>` Елемент містить видимий вміст сторінки
+ `<h1>` Елемент визначає заголовок
+ `<p>` Елемент визначає параграф 

Структура HTML елементу
```html
<p>Paragraph </p>
```
+ `<p>` початковий тег. Містить назва тегу (в даному випадку `p`) в кутових дужках. Цей тег позначає початок елементу, або місце, де елемент починає діяти. У даному випадку — це місце, де починається параграф. 
+ `</p>` кінцевий тег. Такий самий тег, як і початковий, тільки тепер він містить прямий слеш (*forward slash*) перед назвою елементу. Цей тег позначає місце закінчення елементу — у даному випадку, місце, де закінчується параграф. Одна з найпоширеніших помилок початківців — це забути поставити кінцевий тег, що може призвести до несподіваних результатів.
+ `content` вміст елементу, у даному випадку — просто текст "Paragraph".
+ `element` початковий тег плюс кінцевий тег плюс вміст між ними — це і є елемент.   

##### Елемент `DOCTYPE` [desc](https://www.w3schools.com/tags/tag_doctype.asp) [alt](https://ruseller.com/htmlshpora.php?id=21)

Елемент `<!DOCTYPE>` призначений для визначення типу поточного документа - *DTD* (document type definition, опис типу документа). 
Це необхідно, щоб браузер розумів, як слід інтерпретувати поточну веб-сторінку, оскільки HTML існує в декількох версіях, крім того, є *XHTML* (EXtensible HyperText Markup Language, розширена мова розмітки гіпертексту), схожий на HTML, але розрізняються з ним по синтаксису. Щоб браузер «не плутався» і розумів, згідно яким стандартом відображати веб-сторінку і необхідно в першому рядку коду задавати '<! DOCTYPE>'.
Елемент `<! DOCTYPE>` не є тегом HTML, це інструкція веб-браузеру.