console.log("Hello");

function factorial(number) {
    return (number !== 1) ? number * factorial(number - 1) : 1;
}

console.log("Factorial of 5: ", factorial(5));

function checkPalindrom(str) {
    var lower = str.toLowerCase();
    return lower === lower.split('').reverse().join('');
}

console.log("abbA should be palindrome: ", checkPalindrom("abbA"));
console.log("abbb should't be palindrome: ", checkPalindrom("abbb"));

function cutLine(text, toCut) {
    if (text.length > toCut) {
        return (text.substr(0, toCut-3) + "...")
    } else {
        return (text.substr(0, toCut));
    }
}

console.log("Cutline first test: ", cutLine("qwertasdfgzxcvb", 5));
console.log("Cutline second test: ", cutLine("qwertasdfg", 10));

var array = [1, 2, 3, 4, 5, 6, 7, 8];

function rebaseArray(array, maxLength) {
    var newArrayLength = maxLength;

    return array.reduce(function (array, second, third) {
        if (third % newArrayLength === 0) array.push({});
        array[array.length - 1][third] = second;
        return array;
    }, []);
}

console.log("Divided array with max length 3: ", rebaseArray(array, 3));
console.log("Divided array with max length 2: ", rebaseArray([1,2,3,4,5,6,7,8,9,10],2));

var numbersArray = [4, 12, 1, 6, 8];

function findNewPosition(array, number) {
    var newArray = array.sort(function (a, b) {
        return a > b;
    });
    for (var i = 0; i < newArray.length; i++) {
        if (newArray[i] < number && number < newArray[i + 1] ) {
            return i + 1;
        } else if (number < newArray[0]) {
            return 0
        } else  if (number > newArray[newArray.size]){
            return newArray.length;
        }
    }
}

console.log("Should return 2 ", findNewPosition(numbersArray, 5));

function contains(target, comparable) {
    var split = comparable.split("");

    for (var i = 0; i < split.length; i++) {
        if (target.indexOf(split[i]) === -1) {
            return false;
        }
    }
    return true;
}

console.log("contains method result (should be true): ", contains('qwertyuasdfghzxcvbn', 'qweasdzxc'));
console.log("contains method result (should be false): ", contains('qweasd123', 'qwe1234'));

//                    Secret text start

var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

function shiftAlphabet(shift) {
    var shiftedAlphabet = '';
    for (var i = 0; i < alphabet.length; i++) {
        currentLetter = (alphabet[i + shift] === undefined) ?
            (alphabet[i + shift - alphabet.length]) :
            (alphabet[i + shift]);

        shiftedAlphabet = shiftedAlphabet.concat(currentLetter);
    }
    return shiftedAlphabet;
}

function encrypt(message, shift) {
    var shiftedAlphabet = shiftAlphabet(shift);
    var encryptedMessage = '';
    for (var i = 0; i < message.length; i++) {
        var indexOfLetter = alphabet.indexOf(message[i].toUpperCase());
        encryptedMessage = encryptedMessage.concat(shiftedAlphabet[indexOfLetter]);
    }
    return encryptedMessage;
}

function decrypt(message, shift) {
    var shiftedAlphabet = shiftAlphabet(shift);
    var encryptedMessage = '';
    for (var i = 0; i < message.length; i++) {
        var indexOfLetter = shiftedAlphabet.indexOf(message[i].toUpperCase());
        encryptedMessage = encryptedMessage.concat(alphabet[indexOfLetter]);
    }
    return encryptedMessage;
}

var textToEncript = "Hello";
var encryptedHello = encrypt(textToEncript, 13);
console.log('Encrypted "Hello": ', encryptedHello);

var decriptedHello = decrypt(encryptedHello, 13);
console.log('Decripted "Hello": ', decriptedHello);

//                      Secret text end

function createCircle(size, colour) {
    var circle = document.createElement("div");
    var parent = document.getElementsByClassName("circle-container")[0];

    circle.style.width = size+"px";
    circle.style.height = size+"px";
    circle.style.borderRadius = size+"px";
    circle.style.backgroundColor = colour;

    parent.appendChild(circle);
}

function createList(liCount) {
    var ul = document.createElement("ul");
    ul.className = "new-ul";
    document.getElementsByClassName("list-container")[0].appendChild(ul);
    var insertTo = document.getElementsByClassName("new-ul")[0];

    for (var i = 0; i < liCount; i++) {
        var li = document.createElement("li");
        li.innerHTML = prompt("Enter value");
        insertTo.appendChild(li);
    }
}




