fetch('https://ipinfo.io/json')
    .then(function(response){
        return response.json();
    })
    .then(function(ipdata){
        // console.log(ipdata);
        let lat = ipdata.loc.split(',')[0];
        let lon = ipdata.loc.split(',')[1];
        return { lat, lon}
    })
    .then(function(coords){
        fetch(`http://api.openweathermap.org/data/2.5/weather?lat=${coords.lat}&lon=${coords.lon}&appid=bf60c7cca9ba7d27aa20f720b3d78bec`)
            .then(function(weatherresp){
                return weatherresp.json();
            })
            .then(function(weatherdata){
                console.log(weatherdata);
            })
    })