$(document).ready(function () {
        let lat= '';
        let lon = '';
        let city = ';';
    $.get("https://ipinfo.io/json", function (responce) {
        lat = responce.loc.split(',')[0];
        lon = responce.loc.split(',')[1];
        city = responce.city;
        $.get("http://api.openweathermap.org/data/2.5/weather?q="+city+"&appid=bf60c7cca9ba7d27aa20f720b3d78bec", function(res) {
            console.log(res)
            $(".city").html(city);

            let temp = Math.round(res.main.temp) - 273;
            $(".temp").html(temp + '&#8451')

            let weatherName = res.weather[0]['main'];
            console.log(weatherName);
            $(".weather").html(weatherName);

            let weatherIcon = res.weather[0].icon;
            $(".weather-img").attr('src', `http://openweathermap.org/img/w/${weatherIcon}.png`);
        });
    })
});